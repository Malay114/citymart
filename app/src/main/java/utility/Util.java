package utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bean.ListItem;

public class Util {
	public static List<ListItem> jsonTOListItemCategory(JSONObject jsonObj) throws JSONException
	{
		
		JSONArray jsonArray = jsonObj.optJSONArray("info");
		if(jsonArray!=null)
		{
			List<ListItem> listItem = new LinkedList<ListItem>();
			int length = jsonArray.length();  
	        
	        for(int i=0; i < length; i++) 
	        {
	        	ListItem singleItem = new ListItem();
	            JSONObject jsonChildNode = jsonArray.getJSONObject(i);
	            Object jsonId = jsonChildNode.getInt("cate_Id");
	            Object jsonCategoryName=jsonChildNode.get("cate_Name");
	            Object jsonImage = jsonChildNode.get("cate_Image");
	            
	            singleItem.setName(jsonCategoryName.toString());
	            singleItem.setImageData(jsonImage.toString());
	            singleItem.setId(jsonId.toString());
	            listItem.add(singleItem);
	        }
			return listItem;
		}
		else
		{
			return null;
		}
	}
	
	public static List<ListItem> jsontoquestion(JSONObject jsonObj) throws JSONException
	{
		
		JSONArray jsonArray = jsonObj.optJSONArray("info");
		if(jsonArray!=null)
		{
			List<ListItem> listItem = new LinkedList<ListItem>();
			int length = jsonArray.length();  
	        
	        for(int i=0; i < length; i++) 
	        {
	        	ListItem singleItem = new ListItem();
	            JSONObject jsonChildNode = jsonArray.getJSONObject(i);
	            Object jsonId = jsonChildNode.getInt("que_id");
	            Object jsonQuestion=jsonChildNode.get("question");
	          
	            singleItem.setId(jsonId.toString());
	            singleItem.setName(jsonQuestion.toString());
	            
	            listItem.add(singleItem);
	        }
			return listItem;
		}
		else
		{
			return null;
		}
	}
	public static List<ListItem> jsonTOListItemSubCategory(JSONObject jsonObjsubCategory) throws JSONException
	{
		
		JSONArray jsonArraysubCategory = jsonObjsubCategory.optJSONArray("info");
		int lengthsubCategory = jsonArraysubCategory.length();  
        List<ListItem> listItemsubCategory = new LinkedList<ListItem>();
        for(int i=0; i < lengthsubCategory; i++) 
        {
        	ListItem singleItemsubCategory = new ListItem();
            JSONObject jsonChildNode = jsonArraysubCategory.getJSONObject(i);
            Object jsonSubCategoryId=jsonChildNode.get("Subcate_Id");
            Object jsonSubCategoryName=jsonChildNode.get("Subcate_Name");
            //Object jsonSubCategoryDesc = jsonChildNode.get("Subcate_Desc");
            
            Object jsonSubCategoryImage = jsonChildNode.get("Subcate_Image");
            
            /*if(jsonSubCategoryDesc == null)
            {
            	singleItemsubCategory.setDescOrAddress("");
            }*/
            singleItemsubCategory.setId(jsonSubCategoryId.toString());
            singleItemsubCategory.setName(jsonSubCategoryName.toString());
            //singleItemsubCategory.setDescOrAddress(jsonSubCategoryDesc.toString());
            singleItemsubCategory.setImageData(jsonSubCategoryImage.toString());
            listItemsubCategory.add(singleItemsubCategory);    
          }  
		return listItemsubCategory;  
	}
	
	public static List<ListItem> jsonTOListItemFinalResult(JSONObject jsonObjResultList) throws JSONException
	{
		JSONArray jsonArrayResult = jsonObjResultList.optJSONArray("info");
		int lengthsubCategory = jsonArrayResult.length();  
        List<ListItem> listItemResult = new LinkedList<ListItem>();
        
        for(int i=0; i < lengthsubCategory; i++) 
        {
        	ListItem singleIteResult = new ListItem();
            JSONObject jsonChildNode = jsonArrayResult.getJSONObject(i);
            
            Object jsonId=jsonChildNode.get("shop_id");
            Object jsonType=jsonChildNode.get("shop_type");
            Object jsonName=jsonChildNode.get("Name");
            String jsonLat = jsonChildNode.get("Latitude").toString();
            String jsonLng = jsonChildNode.get("Longitude").toString(); 
            Object jsonKm = jsonChildNode.get("distance");
            Object jsonTime = jsonChildNode.get("duration");
//            String jsonOffer=jsonChildNode.getString("Offer");
//            if(jsonOffer.equals("null"))
//            {
//            	jsonOffer="No offer";
//            	Log.e("","))))))))))))))"+jsonOffer);
//            }
            String jsonOffer="No offer";
//            Object jsonRating=jsonChildNode.get("Rating");
//            Object jsonCount=jsonChildNode.get("Counter");
//            Log.e("util","jsonCount"+jsonCount);
//            float rating=Float.parseFloat( jsonRating .toString());
			float rating=4;
//            int counter=Integer.parseInt(jsonCount.toString());
			int counter=10;
            Log.e("utility","counter"+counter);
            Double lnt= Double.parseDouble(jsonLat);
            Double lng= Double.parseDouble(jsonLng);
            
           
           String  distance = jsonKm.toString();
           Matcher matcher = Pattern.compile("[-+]?\\d*\\.?\\d+([eE][-+]?\\d+)?").matcher( distance );
           while ( matcher.find() )
           {
               double dis = Double.parseDouble( matcher.group() );
               singleIteResult.setKm(dis);
           }
           
           singleIteResult.setId(jsonId.toString());
           singleIteResult.setType(jsonType.toString());
           singleIteResult.setName(jsonName.toString());
           singleIteResult.setTime(jsonTime.toString());
           singleIteResult.setLatitude(lnt);
           singleIteResult.setOffers(jsonOffer);
           singleIteResult.setLongitude(lng);
           singleIteResult.setTime(jsonTime.toString());
           singleIteResult.setRating(rating);
           singleIteResult.setCount(counter);
            listItemResult.add(singleIteResult);    
          }  
        
		return listItemResult;  
	}
	public static ListItem jsonTOLocationAddress(JSONObject jsonObjAddress) throws JSONException
	{
		Log.e("Util","jsonObjAddress"+jsonObjAddress);
		    JSONArray jsonArrayResult = jsonObjAddress.optJSONArray("info");
		    JSONObject jsonChildNode = jsonArrayResult.getJSONObject(0);
        	ListItem singleIteResult = new ListItem();
            Object jsonName=jsonChildNode.get("Name");
            Object jsonAddress = jsonChildNode.get("Address");
            Object jsonLandmark = jsonChildNode.get("Land_Mark");
            Object jsonCity=jsonChildNode.get("City");
            Object jsonPincode = jsonChildNode.get("Pincode");
            String jsonLat = jsonChildNode.get("Latitude").toString();
            String jsonLng = jsonChildNode.get("Longitude").toString();
            Object jsonKm = jsonChildNode.get("Distance");
            Object jsonTime = jsonChildNode.get("Duration");
           
            
            String  distance = jsonKm.toString();
            Log.e("Util","distance"+distance);
            Matcher matcher = Pattern.compile("[-+]?\\d*\\.?\\d+([eE][-+]?\\d+)?").matcher( distance );
            while ( matcher.find() )
            {
                double dis = Double.parseDouble( matcher.group() );
                Log.e("Util","dis"+dis);
                singleIteResult.setKm(dis);
            }
       
            Double lnt= Double.parseDouble(jsonLat);
            Double lng= Double.parseDouble(jsonLng);
            
            singleIteResult.setTime(jsonTime.toString());       
            String address=jsonAddress+"\n"+jsonLandmark+"\n"+jsonCity+" - "+jsonPincode;
            singleIteResult.setName(jsonName.toString());
            singleIteResult.setDescOrAddress(address);
            singleIteResult.setLatitude(lnt);
            singleIteResult.setLongitude(lng);
            return singleIteResult;  
	}
	public static Bitmap StringToBitmap(String imageData)
	{
		  byte[] decodedString=null;
		  Bitmap decodedByte=null;
		  decodedString = Base64.decode(imageData, Base64.DEFAULT);
		  decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
		  return decodedByte;
		
	}
}
