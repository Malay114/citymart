package utility;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences 
{
	
	String MyPREFERENCES="Preferences";
	Context context;
	SharedPreferences sharedpreferences;
	SharedPreferences.Editor spOptionEditor;
	static Preferences preferences;
	
	public Preferences(Context context)
	{
		this.context=context;
	
		//sharedpreferences = this.context.getSharedPreferences(MyPREFERENCES, Context.MODE_WORLD_READABLE);
		sharedpreferences = this.context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
	}
	public String getString(String key,String defaultValue)
	{
		return sharedpreferences.getString(key, defaultValue); 
	}
	public void setString(String key,String value)
	{
		spOptionEditor = sharedpreferences.edit();
		spOptionEditor.putString(key, value);
	}

	public static void setPreferences(Preferences i_prePreferences)
	{
		preferences = i_prePreferences;
	}
	public static Preferences getPreferences()
	{
		return preferences;
	}
}
