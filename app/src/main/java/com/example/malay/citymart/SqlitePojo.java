package com.example.malay.citymart;


public class SqlitePojo
{
    public String city;
    public int id;


    public SqlitePojo() {
        super();
    }

    public SqlitePojo(String city) {
        super();
        this.city = city;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

}

