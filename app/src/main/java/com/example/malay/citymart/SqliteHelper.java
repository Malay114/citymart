package com.example.malay.citymart;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import bean.ListItem;


public class SqliteHelper extends SQLiteOpenHelper {
    public static final String TAG = "SqliteHelper.java";

    public static final String City = "City";
    public static final String COLUMN_ID="id";
    public static final String COLUMN_CityName="CityName";
    public static final String DATABASE_NAME="citydatabased.db";
    private static final int DATABASE_VERSION = 2;
    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        final String DATABASE_CREATE="create table "+City+"(" + COLUMN_ID + " Integer Primary Key," + COLUMN_CityName
                +" text not null"+")";
        database.execSQL(DATABASE_CREATE);
        //database.execSQL("DROP TABLE IF EXISTs Login");

        final String LoginTable="create table IF NOT EXISTS Login(id Integer Primary Key autoincrement,UserName text not null,Password text not null,UserType text not null,Active text)";
        database.execSQL(LoginTable);

        final String registration="create table IF NOT EXISTS Registration(user_id Integer Primary Key autoincrement,Name text not null,mobile_No text not null,email_Address text not null,password text not null,question_1 integer not null,ans_1 text not null,question_2 integer not null,ans_2 text not null, ownertype not null,user_type text not null,Active text not null)";
        database.execSQL(registration);

        final String Rating_table="create table IF NOT EXISTS Rating(id Integer ,shop_type text not null)";
        Log.e("","Table rating create.......................");
        database.execSQL(Rating_table);
    }
    public void droptable()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTs Login");
    }
    public void deleteRecords(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from Login");
    }

    public void InsertLogin(String uname, String password)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query="insert into Login (UserName,Password,UserType,Active)values('"+uname+"','"+password+"',2,'Active')";
        db.execSQL(query);
    }
    public void InsertRating(int id ,String shop_type)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query="insert into Rating (id,shop_type)values("+id+",'"+shop_type+"')";
        db.execSQL(query);
    }
    public void InsertRegistration()
    {
        //String query="insert into Registration(Name,mobile_No,email_Address,password,question_1,ans_1,question_2,ans_2, ownertype,user_type,active) values(
    }

    public String getEmail()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();
        String uname=null;
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            uname = cursor.getString(cursor.getColumnIndex("UserName"));
        }
        else
        {
            uname="";
        }
        return uname;

    }

    public String ActiveOrNot()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);
        String status=null;
        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            status = cursor.getString(cursor.getColumnIndex("Active"));

        }
        else
        {
            status="";
        }
        return status;

    }
    public String GetPassword()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);
        String pwd=null;
        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            pwd = cursor.getString(cursor.getColumnIndex("Password"));

        }
        else
        {
            pwd="";
        }
        return pwd;

    }
    public int getShopId(int shop_id)
    {
        String sql = "";
        sql += "SELECT * FROM Rating";

        SQLiteDatabase db = this.getWritableDatabase();
        int id=0;
        Cursor cursor = db.rawQuery(sql, null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            do
            {
                int objectid = cursor.getInt(cursor.getColumnIndex("id"));
                if(objectid==shop_id)
                {
                    id=shop_id;
                    break;
                }

            } while (cursor.moveToNext());
        }
        return id;
    }
    public String getShopType(String type)
    {
        String sql = "";
        sql += "SELECT * FROM Rating";
        SQLiteDatabase db = this.getWritableDatabase();
        String shop_type="";
        Cursor cursor = db.rawQuery(sql, null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            do
            {
                shop_type = cursor.getString(cursor.getColumnIndex("shop_type"));
                if(shop_type.equals(type))
                {
                    shop_type=type;
                    break;
                }

            } while (cursor.moveToNext());
        }
        return shop_type;
    }


    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + City);
        onCreate(database);
    }
    public void ifexist()
    {

        SQLiteDatabase database=this.getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS " + City);
        onCreate(database);
    }

    public boolean createCity(MyObject myObj) {

        boolean createSuccessful = false;

        if(!checkIfExists(myObj.objectName)){

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, myObj.objectId);
            values.put(COLUMN_CityName, myObj.objectName);

            createSuccessful = db.insert(City, null, values) > 0;

            db.close();

        }
        return createSuccessful;
    }

    public SqlitePojo displayCity(String city)
    {
        SQLiteDatabase database=this.getReadableDatabase();
        Cursor cursor=database.query(City, new String[] {COLUMN_CityName}, COLUMN_CityName +"=?", new String[] {String.valueOf(city)}, null, null, null, null);

        if(cursor!=null)

            cursor.moveToFirst();

        SqlitePojo pojo=new SqlitePojo(cursor.getString(0));

        return pojo;
    }
    // check if a record exists so it won't insert the next time you run this code
    public boolean checkIfExists(String objectName){

        boolean recordExists = false;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + COLUMN_CityName + " FROM " + City + " WHERE " + COLUMN_CityName + " = '" + objectName + "'", null);

        if(cursor!=null) {

            if(cursor.getCount()>0) {
                recordExists = true;
            }
        }

        cursor.close();
        db.close();

        return recordExists;
    }

    public List<SqlitePojo> displayAllCity()
    {
        List<SqlitePojo> cityListPojo=new ArrayList<SqlitePojo>();
        String selectQuery="Select * from "+City;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst())
        {
            do{
                SqlitePojo pojo=new SqlitePojo();
                pojo.setCity(cursor.getString(0));

                cityListPojo.add(pojo);

            } while (cursor.moveToNext());
        }
        return cityListPojo;
    }


    public void deleteCity(String city) {
        SQLiteDatabase db = this.getWritableDatabase();
        SqlitePojo pojo=new SqlitePojo();
        db.delete(City, COLUMN_CityName + " = ?",
                new String[] { String.valueOf(pojo.getCity()) });
        db.close();
    }

    // Read records related to the search term
    public List<MyObject> read(String searchTerm) {

        List<MyObject> recordsList = new ArrayList<MyObject>();

        // select query
        String sql = "";
        sql += "SELECT * FROM " + City;
        sql += " WHERE " + COLUMN_CityName + " LIKE '%" + searchTerm + "%'";
        sql += " ORDER BY " + COLUMN_ID + " DESC";
        sql += " LIMIT 0,5";

        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objectName = cursor.getString(cursor.getColumnIndex(COLUMN_CityName));
                int objectId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
                MyObject myObject = new MyObject(objectId,objectName);

                // add to list
                recordsList.add(myObject);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

    public int getId(String searchTerm) {

        // select query

        String sql = "";
        sql += "SELECT id FROM " + City;
        sql += " WHERE " + COLUMN_CityName + " LIKE '%" + searchTerm + "%'";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);
        cursor.moveToFirst();
        int objectId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));

        cursor.close();
        db.close();
        return objectId;
    }
    public List<ListItem> getCityName() {

        List<ListItem> recordsList = new ArrayList<ListItem>();

        // select query
        String sql = "";
        sql += "SELECT * FROM " + City;

        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ListItem singleItem = new ListItem();
                String cityName = cursor.getString(cursor.getColumnIndex(COLUMN_CityName));

                singleItem.setName(cityName);
                // add to list
                recordsList.add(singleItem);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

}
