package com.example.malay.citymart;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import bean.ListItem;
import utility.Path;
import utility.Util;


public class ShopAddressFragment extends Fragment {
    View view;
    private ProgressDialog progress;
    private String Shop_Name;
    private String cityName;
    private String Subcategory_id;
    private String category_id;
    private ListItem listobj;
    private Double dest_Longitude;
    private Double dest_Latitude;
    private float db_rating;
    private int count;
    private String Shop_Id,Shop_Type,serverName;
    private SqliteHelper sqliteHelper;
    TextView txtAddress,txtdistance,txtname;
    ImageView imageView ;
    RatingBar ratingbar1;
    Button btn_rate;
    int city_id;
    FragmentManager fragmentManager;
    FragmentTransaction ft;
    Fragment frag_Shop_list;
    Bundle args;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.shop_address, container, false);
        Subcategory_id = getArguments().getString("Subcategory_Id");
        Shop_Name = getArguments().getString("Shop_Name");
//        category_id = getArguments().getString("Cate_Id");
        cityName = getArguments().getString("City_Name");
        city_id=getArguments().getInt("city_id");
        Shop_Type = getArguments().getString("Shop_Type");
        Shop_Id = getArguments().getString("Shop_Id");
        db_rating=getArguments().getInt("Rating",0);
        count=getArguments().getInt("Counter", 0);


        dest_Longitude=getArguments().getDouble("Longitude",0);
        dest_Latitude=getArguments().getDouble("Latitude",0);

        args = new Bundle();
        fragmentManager= getFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();

        GetCurrentLocation getlocation=new GetCurrentLocation();
        Context context=getContext();
        getlocation.turnGPSOn(context); // method to turn on the GPS if its in off state.
        getlocation.getMyCurrentLocation(context);
        Double User_latitude =getlocation.MyLat;
        Double User_longitude =getlocation.MyLong;
        getlocation.turnGPSOff(context);


        txtAddress = (TextView)view.findViewById(R.id.txt_address);
        txtdistance=(TextView)view.findViewById(R.id.txt_distance);
        imageView = (ImageView)view.findViewById(R.id.imageView_mapNavigation);
        txtname=(TextView)view.findViewById(R.id.txt_shop_name);
        ratingbar1=(RatingBar)view.findViewById(R.id.shop_rating);

        serverName= Path.getPath();
        //String str_url=serverName+"get_shop_list_for_subcategory.php?city_id=1&category_id="+category_id+"&subcategory_id="+Subcategory_id;
        //String str_url=serverName+"get_shop_list_for_subcategory.php?city_id=1&category_id="+category_id+"&subcategory_id="+Subcategory_id+"&user_latitude="+Latitude+"&user_longitude="+Longitude;
        String str_url=serverName+"get_final_shop_detail_distance.php?city_id="+city_id+"&shop_id="+Shop_Id+"&shop_type="+Shop_Type+"&user_latitude="+User_latitude+"&user_longitude="+User_longitude;
        //String str_url="http://spcitymart.host22.com/citymartwebservice/get_final_shop_detail_distance.php?city_id=2&shop_id=118&shop_type=Simple%20Shop&user_latitude=20.9496821&user_longitude=72.9487415";


        progress = new ProgressDialog(getActivity());
        progress.setMessage("Loading... ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progress.setIndeterminate(true);
        progress.show();


        new Asynkclass().execute(str_url);

        btn_rate=(Button)view.findViewById(R.id.btn_rate);

        sqliteHelper=new SqliteHelper(getActivity());
        int id=sqliteHelper.getShopId(Integer.parseInt(Shop_Id));
        String shop_type=sqliteHelper.getShopType(Shop_Type);
        if(shop_type.equals(Shop_Type) && id== Integer.parseInt(Shop_Id))
        {
            btn_rate.setVisibility(View.GONE);
        }
        btn_rate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                ShowDialog();

            }
        });
        return view;
    }
    public void ShowDialog()
    {

        float rating= ratingbar1.getRating();
        rating=((db_rating*count)+rating)/(count+1);
        count=count+1;
        String str_url=serverName+"get_rating.php?rating="+rating+"&shop_id="+Shop_Id+"&shop_type="+Shop_Type+"&Counter="+count;
        ratingbar1.setRating(rating);
        int id= Integer.parseInt(Shop_Id);
        sqliteHelper.InsertRating(id, Shop_Type);
        btn_rate.setVisibility(View.GONE);
        new addreAynkClass().execute(str_url);


//        final AlertDialog.Builder popDialog = new AlertDialog.Builder(getActivity());
//        final RatingBar rating = new RatingBar(getActivity());
//        rating.setMax(4);
//        popDialog.setIcon(android.R.drawable.btn_star_big_on);
//        popDialog.setTitle("Rate me ");
//        popDialog.setView(rating);
//        // Button OK
//        popDialog.setPositiveButton(android.R.string.ok,
//                new DialogInterface.OnClickListener()
//                {
//                    public void onClick(DialogInterface dialog, int which)
//                    {
//
//                        float rating= ratingbar1.getRating();
//                        rating=((db_rating*count)+rating)/(count+1);
//                        count=count+1;
//                        ratingbar1.setRating(rating);
//                        String str_url=serverName+"get_rating.php?rating="+rating+"&shop_id="+Shop_Id+"&shop_type="+Shop_Type+"&Counter="+count;
//                        ratingbar1.setRating(rating);
//                        int id=Integer.parseInt(Shop_Id);
//                        sqliteHelper.InsertRating(id, Shop_Type);
//                        btn_rate.setVisibility(View.GONE);
//                        new addreAynkClass().execute(str_url);
//                        dialog.dismiss();
//                    }
//                })
//                .setNegativeButton("Cancel",
//                        new DialogInterface.OnClickListener()
//                        {
//                            public void onClick(DialogInterface dialog, int id)
//                            {
//                                dialog.cancel();
//                            }
//                        });
//
//        popDialog.create();
//
//        popDialog.show();

    }
    private class addreAynkClass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls) {
            // TODO Auto-generated method stub
            StringBuilder sb = new StringBuilder();

            try
            {
                URL obj=new URL(urls[0]);
                URLConnection conn=obj.openConnection();
                //send request
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
    }
    private class Asynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }
        @Override
        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            listobj = new ListItem();

            try
            {
                jsonResponse = new JSONObject(result);
                listobj= Util.jsonTOLocationAddress(jsonResponse);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();
            imageView.setImageResource(R.mipmap.google_maps);
            RatingBar ratingbar=(RatingBar)view.findViewById(R.id.shop_rating);

            if(listobj!=null)
            {
                txtname.setText(listobj.getName());
                txtdistance.setText(""+listobj.getKm()+" Km");
                txtAddress.setText(listobj.getDescOrAddress());
                ratingbar.setRating(db_rating);

            }
            else
            {
                Toast.makeText(getContext(),"No data to display", Toast.LENGTH_LONG).show();
            }

            imageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View arg0, MotionEvent arg1) {
                    // TODO Auto-generated method stub
                    Intent ShopAddressActivityIntent = new Intent(getActivity(), MapsActivity.class);
                    ShopAddressActivityIntent.putExtra("duration",listobj.getTime());
                    ShopAddressActivityIntent.putExtra("Km",listobj.getKm());
                    ShopAddressActivityIntent.putExtra("Latitude",dest_Latitude);
                    ShopAddressActivityIntent.putExtra("Longitude",dest_Longitude);
                    startActivity(ShopAddressActivityIntent);
                    return false;
                }
            });
        }
    }
}
