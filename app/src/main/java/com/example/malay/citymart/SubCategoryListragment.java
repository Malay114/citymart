package com.example.malay.citymart;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import bean.ListItem;
import utility.Path;
import utility.Util;


public class SubCategoryListragment extends Fragment {
    View view;
    private ProgressBar pb;
    private ListView list;
    private List<ListItem> listobj= new ArrayList<ListItem>();
    private String Category_Name;
    private String Category_Id;
    int cityId;
    private String ImageData;
    GridView gridView;


    FragmentManager fragmentManager;
    FragmentTransaction ft;
    Fragment frag_Shop_list;

    Bundle args;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.category_custom_list, container, false);

        Category_Name = getArguments().getString("Category_Name");
        Category_Id = getArguments().getString("Category_Id");
        ImageData=getArguments().getString("imageData");
        cityId=getArguments().getInt("city_id",0);

        args = new Bundle();
        fragmentManager= getFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();
//        list=(ListView)view.findViewById(R.id.list11);
        gridView=(GridView)view.findViewById(R.id.gridView1);


        if(listobj.isEmpty())
        {
            pb = (ProgressBar) view.findViewById(R.id.pbLoading);
            pb.setVisibility(ProgressBar.VISIBLE);
            String serverName= Path.getPath();
            String str_url=serverName+"get_available_subcate_city.php?city_id="+cityId+"&category_id="+Category_Id;
            new Asynkclass().execute(str_url);
        }
        else
            onSuccess();

        return view;
    }
    private class Asynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }
        @Override
        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            //result="{"info":[{"Subcate_Id":"1","Subcate_Name":"Mobiles","Subcate_Image":"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAPhJREFUWIXtlzFqwzAUhr+aTq3f4oJvYHwE08VTwWuWHqMX8WDw6iFjoPfyBQyOiBskdUgLiduSl5DiRT9o0OPT44OnQYKQhXN3AfcKJEp+B2yAj3PgvbLhM\/D+vYnjGBE5AYwxDMNwXIqA9a0EHo43aZqSZdkJ0Pf9XOBR01grQFVVtG2rYpumoes6FasWEBHyPFexSaK9Koc5LZogEASCQBAIAkEgCCwuoH6QOOeYpknFWmuvFvorL4Cfr6IofFmWP+pf6+1fBaIo8sYYb631InK1gHYEZl5wzlHXNSLCOI6\/ndlqGl\/yMVkBT0p+x+EfsVfyIcvlE0wFTeU6AUEKAAAAAElFTkSuQmCC"},{"Subcate_Id":"2","Subcate_Name":"Laptops","Subcate_Image":"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAHl0lEQVR4XsWUW6glV7WGvzHnrFpr7e7eu\/vs7t1203qiBz1NLiAE1GCCaBBfEg9ewftB8E1MIoISfPDNGOUQ8MWXQB5ERSQQfPAg3hAxRhIjYidgTDod09edvvdeq6rmHGO4nRTdC5uAIOi3+Blz1aTqH6P4Z8n+g+8GN9wzofk0pP9DdIIQSVEoCimCGhUcQhAEmDTCoCCAO8QIZhACFIUAqEMQaBpjXjrQz3P8xY8DEOUwyc04ffInXONT\/KswMkGHjn8X+9YPkt74nk8wwl133eaL9ZtRFVKbUDPaEAgC0R03I8SGPGTcHQjgVtchBLQopoq54ua4gwAxBBxoYmA48zQ\/\/8XvBWDvbe8lnb98khH2vOGtvHDkz3jJ1VRCwB3EHXfD1FGttZpWxoI7PtaqcSsgiEAeCideOMKtb3k7Iyz6V0iUKSN0iwVb23r04Ue4dc+MPw3w4DNn2CWgQyF3mdIVci6YFlyNKjO0FFxBbGxOYdI2dN3Ac88f59ixM3DqFSQoVylTUgjCCEPOlKHw36sNtM5e4Mzp81xQQA3LipmDKlQjQ9zGiRUs1mvJIybOyyfP8uKLpymLgdk0sRWEIMIyiSUkBNjWfT98gpdfOs6hGw7h7TpzMwKOiaBasKFANtQUKYblAc0ZKUoMkUuXF2xuXqDvCylFJslr8+JOgFdvAHP6buCJ+RQOHOaURDZmLU2bkCAkE6Qv6FZP3lowXJ6jJWOqhBgo2bh4\/grzi3NEhEkTsH5A8wJdzLFSahPXEBLLxEAPvO\/ghA8dSPzg2GW+fxH2TiKBgBXDhlJFUYo4Fozc9yyudPSLAZFAs9LCUHACYSoQwZG6J0EYwXASxjVSIrvwptcd5Oa3vZbn0gt866nTrCAELZR5xrY6dNHjOZMXC3wx0PcdFEMMfMhYUbwULGfcMq6KXz2TSzM7JAlcpfxtigtbxHkPYcaKCFeObnJlbUZ06kM9F7TLWO7RboChR9QRj3gQ4izhtZEeQkAHR1Qppcc0I5ZZJiFcw4yohe\/94gihSXz3p3+k2TxLns\/QAG5Sp6EU3BXRgmE1vNIEgkfqoG4ILSZKIKHe1aC6Gfb3GXCWIyBEgcd++SyP\/ej3\/Mfr99OkiC4WdToMRG380mlNvbshIdXmiAYecHdclFqt4GrgVALCCCJOYOmCOwzdwJc\/ejv+1AOc+vZnuXLuAqXr8WEALagXUEXUcQQIIIGKCxQDrRkAs9ogXgCDMQfLBHBGcFUYMjce3A3A8R76S3O062vyfchIdkwdMwP82kQ1I4rXhVcvFwM33B1fsnz174ALmPGR99wCwI+fOEGTAsHH7oOACMEFcxAPgIBardX46ic6w1BqcHEHNQCCLNk5pOVMhJSQtuHD93+Hc1vK754\/zeraLkJqEAJmAcEx1XFaBReICQk1D3jOWCnU0OUOKwOmBTeFmgljmeRhzggxTUntjEef2iSFwM6du0mTFg9QEMDBnJruWYJJQlKEKLg59WcKQ4bFvIbX5tsKHSoNHhLEKSN46EhadjFCd\/EUu1rl8H8GUozEqEjsiAAY4kCE+kaSQczgeQydYe7U5O8EPGGs4uykFEX7nmdPTcnDFUZYzBOyd9+tbJ55EoA333SLnz13oWsam8IYNI+dBN9SDYKAOMgYLjcQAYJjGklJcQTXSEi44CsuYRZChBCQQL9jOp2cOb\/7Brd8zN2Qx3\/9MxBIqbmz2K7\/P3n8xNbKzNbcQUKkTe1lkXC5GIIVVB2XDFbqZIIQU0A10c4UIaGWiPWQ6w4tvqZaEBGccLGZTGev2Vj9StvMvlq0IN948OvceONNzHbu+Mv\/fvJjhyQKeGR97zp939OkRM6ZEAIxRooOJGkopgD1mqqSUkTH4xliqMFrUoupQ5Kaqc3NTc6fO8sXvvgl7r77f2QYepID7mx87YGvHjr20suMcHbzFRZ9x3Q6ZRiGarStum6ahsViQUoJd6\/GTYrEFGujqgpmxJRQLbTthH4YCAjzbsE3H3qIO+54x+2\/efzxX6UYYzh9dvP+z913L+99\/weoBilhbnhRQoy0kxbTjJtTSmHf3n00TcLUcIEg1ZRz589hDoRAlAh4bQjABXDq\/YcOHeLkqVP3TCaTZ9L+\/fvv3X\/gwD13vuudbC06spb6QHcFFwTIJaNmmGndi1FwN7RobbSJqZrmYrRNXSNEECoigruPp9jYu3uNX\/\/2yQ+eOHHiqKxMZzetr69\/Zv\/Gvnu6eUefO2q4HCSl+kARIcaG1CSmbYvEQM3EtiRKNXNTzMBqOAvDMFTlvF37TB4G+jwQgH0bGxw\/cfKR85cuPpzW1taO7Nmz594nn376AeC\/RKQ9fPhwu7Gx0ezZvTZZXdvdrK6uyspsZTKbzabtpE2padq2aVIIIYXaDe5m4u5Ft8k5a9912g9D2a79fLHot7a2hkuXLg1Hjx6dm9kftubzkwBpG7YFcIptmRlLtMAqsAvYOWrHqMm4nwABHMijOmBr1OWxXhprzxIJYDTF3VkiArJ00wUgAHFpL4x1BAdsrDrWMsqWPMv1DVyPAQPgSyZyneE\/9t+W95ZJZjZOfh1+\/Rrnn+M6o78Cmd7uP5cj0U4AAAAASUVORK5CYII="}";
            listobj = new ArrayList<ListItem>();
            try
            {
                jsonResponse = new JSONObject(result);
                listobj= Util.jsonTOListItemSubCategory(jsonResponse);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            pb.setVisibility(ProgressBar.INVISIBLE);
            if(listobj!=null) {onSuccess();}
            else
            {
                Toast.makeText(getContext(),"No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }
    private void onSuccess(){
        CategoryListAdapter adapter = new CategoryListAdapter(getActivity(), listobj);
       gridView.setAdapter(adapter);
//        GridViewCategoryAdapter adapter = new GridViewCategoryAdapter(getActivity(), listobj);
//        gridView.setAdapter(adapter);

       gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                // TODO Auto-generated method stub
                frag_Shop_list=new ShopResultListFragment();
                args.putString("Subcategory_Id",listobj.get(position).getId().toString());
                args.putString("Subcategory_Name",listobj.get(position).getName().toString());
                args.putInt("city_id",cityId);
                frag_Shop_list.setArguments(args);
                fragmentManager.beginTransaction()
                        .replace(R.id.content_main, frag_Shop_list )
                        .addToBackStack("")
                        .commit();
            }
        });
    }
}
