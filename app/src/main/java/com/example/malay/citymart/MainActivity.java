package com.example.malay.citymart;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import bean.ListItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    CustomAutoComplete myAutoComplete;
    FragmentManager fragmentManager;
    FragmentTransaction ft;
    public static int cityId=0;
    public static String city;

    // adapter for auto-complete
    ArrayAdapter<String> myAdapter;
    Fragment frag_Cat_list,frag_title;
    Bundle args;

    // just to add some initial value
    String[] item = new String[] {"Please search..."};

    SqliteHelper sqliteHelper;
    EditText cityNameEdit;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        Context context=getApplicationContext();
//        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

//        InputManager im=(InputManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        args = new Bundle();
        fragmentManager= getSupportFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        RelativeLayout img = (RelativeLayout) findViewById(R.id.layoutBottom);
        img.startAnimation(slideUp);

        Animation slidedown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        cityNameEdit = (EditText) findViewById(R.id.city_name);
        cityNameEdit.startAnimation(slidedown);

        Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        ImageView overlapImage = (ImageView) findViewById(R.id.overlapImage);
        overlapImage.startAnimation(fadeIn);

        ImageView autodetectCity=(ImageView)findViewById(R.id.gpsicon);
        autodetectCity.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                // TODO Auto-generated method stub
                GetCurrentLocation getlocation=new GetCurrentLocation();
                Context context=getApplicationContext();
                getlocation.turnGPSOn(context); // method to turn on the GPS if its in off state.
                getlocation.getMyCurrentLocation(context);
                String cityName=getlocation.CityName;
                if(cityName.equals(getlocation.city_name))
                {
                    cityNameEdit.setText(cityName);
                }
                else
                {
                    Toast.makeText(context.getApplicationContext(), "City is not available in app", Toast.LENGTH_LONG).show();
                }
                getlocation.turnGPSOff(context);

                return false;
            }
        });

        try{
            sqliteHelper =new SqliteHelper(MainActivity.this);
            // autocompletetextview is in activity_main.xml
            myAutoComplete = (CustomAutoComplete) findViewById(R.id.city_name);

            // add the listener so it will tries to suggest while the user types
            myAutoComplete.addTextChangedListener(new CustomAutoCompleteTextChangeListener(this));

            // set our adapter
            myAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, item);
            myAutoComplete.setAdapter(myAdapter);

        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        final SqliteHelper sqlitehelper = new SqliteHelper(MainActivity.this);
        myAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                //String data=(String)arg0.getItemAtPosition(arg2);
                String city_name=(String)arg0.getItemAtPosition(0);
                int id=sqlitehelper.getId(city_name);
                cityId=sqlitehelper.getId(city_name);
                frag_Cat_list=new CategoryListragment();
                args.putString("city_name",cityNameEdit.getText().toString());
                city=cityNameEdit.getText().toString();
                args.putInt("city_id",id);
                frag_Cat_list.setArguments(args);
                fragmentManager.beginTransaction()
                        .replace(R.id.content_main, frag_Cat_list )
                        .addToBackStack("")
                        .commit();
            }

        });

   }
    public String[] getItemsFromDb(String searchTerm){

        // add items on the array dynamically
        List<MyObject> products = sqliteHelper.read(searchTerm);
        int rowCount = products.size();

        String[] item = new String[rowCount];
        int x = 0;

        for (MyObject record : products) {

            item[x] = record.objectName;
            x++;
        }

        return item;
    }
    public String getCityFromDb(String searchTerm){

        // add items on the array dynamically
        List<ListItem> products = sqliteHelper.getCityName();
        int rowCount = products.size();

        String City = new String();

        for (int i=0;i<rowCount;i++)
        {
            ListItem singleItem=products.get(i);
            if(singleItem.getName().equals(searchTerm))
            {
                City=singleItem.getName();
            }
        }
        return City;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Intent newAct=new Intent();

        if(id==R.id.nav_home) {
            Intent homeListActivityIntent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(homeListActivityIntent);
        }
        else if(id==R.id.nav_category) {
            frag_title=new CategoryListragment();
            args.putString("city_name","");
            args.putInt("city_id",cityId);
            frag_title.setArguments(args);
            ft=fragmentManager.beginTransaction();
            ft.replace(R.id.content_main, frag_title);
            ft.addToBackStack("");
            ft.commit();
        }
        else if(id==R.id.nav_login) {
            Intent homeListActivityIntent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(homeListActivityIntent);
        }
        else if(id==R.id.nav_logoff){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //Setting message manually and performing action on button click
            builder.setMessage("Do you want to close this application ?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SqliteHelper sqliteHelper =new SqliteHelper(MainActivity.this);
                            sqliteHelper.deleteRecords();
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });

            //Creating dialog box
            AlertDialog alert = builder.create();
            //Setting the title manually
            alert.setTitle("LogOut");
            alert.show();
        }
        //else if(id==R.id.nav_cntus) {newAct = new Intent(Home.this, Home.class);}



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
