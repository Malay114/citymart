package com.example.malay.citymart;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import bean.ListItem;
import utility.Path;
import utility.Util;


public class CategoryListragment extends Fragment {
    View view;
    private ProgressBar pb;
    String cityName;
    static int cityId;
    GridView gridView;
    FragmentManager fragmentManager;
    FragmentTransaction ft;
    Fragment frag_SubCat_list;
    Bundle args;

    private List<ListItem> listobj = new ArrayList<ListItem>();
    public static List<ListItem> Drawer_listobj;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.grid_view_activity, container, false);

        cityName=getArguments().getString("city_name");
        cityId=getArguments().getInt("city_id",0);

        args = new Bundle();
        fragmentManager= getFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();

        gridView=(GridView)view.findViewById(R.id.gridView1);

        if(listobj.isEmpty())
        {
            pb = (ProgressBar) view.findViewById(R.id.pbLoading);
            pb.setVisibility(ProgressBar.VISIBLE);
            String serverName= Path.getPath();
            String str_url=serverName+"get_available_cate_city.php?city_id="+cityId;
            new Asynkclass().execute(str_url);
        }
        else
            onSuccess();

        return view;
    }
    private class Asynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
        @Override
        protected void onPostExecute(String result)
        {

            JSONObject jsonResponse;
            try
            {
                jsonResponse = new JSONObject(result);

                listobj= Util.jsonTOListItemCategory(jsonResponse);
                Drawer_listobj=listobj;
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            pb.setVisibility(ProgressBar.INVISIBLE);

            if(listobj!=null) {onSuccess();}
            else
            {
                Toast.makeText(getContext(),"No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }
    public void onSuccess(){
        GridViewCategoryAdapter adapter = new GridViewCategoryAdapter(getActivity(), listobj);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                // TODO Auto-generated method stub
                frag_SubCat_list=new SubCategoryListragment();
                args.putString("ImageData",listobj.get(position).getImageData().toString());
                args.putString("Category_Id",listobj.get(position).getId().toString());
                args.putString("Category_Name",listobj.get(position).getName().toString());
                args.putInt("city_id",cityId);
                frag_SubCat_list.setArguments(args);
                fragmentManager.beginTransaction()
                        .replace(R.id.content_main, frag_SubCat_list )
                        .addToBackStack("")
                        .commit();
            }
        });
    }
}
