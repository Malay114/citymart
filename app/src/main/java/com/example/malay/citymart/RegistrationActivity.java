package com.example.malay.citymart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import utility.Path;


public class RegistrationActivity extends Activity {

    private ProgressDialog progress;
    String cityName;
    int cityId;
    EditText edtUserName,edtPassword,edtName,edtPhoneNo,edtEmail;
    static String name="",password="",phone="",email="";
    static String[]question1;
    static String[]question2;
    static int que1_id[];
    static int que2_id[];
    TextView txtmessage;
    Button btn_next,btn_cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.registration_personal_info);

        edtName=(EditText)findViewById(R.id.edt_name);
        edtPhoneNo=(EditText)findViewById(R.id.edt_phone_no);
        edtPassword=(EditText)findViewById(R.id.edt_Password);
        edtEmail=(EditText)findViewById(R.id.edt_email);
        txtmessage=(TextView)findViewById(R.id.txtMassage);

        btn_cancel=(Button)findViewById(R.id.btn_cancel);
        btn_next=(Button)findViewById(R.id.btn_next);

        String serverName= Path.getPath();
        String str_url=serverName+"get_question.php";
        new Asynkclass().execute(str_url);
        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {isFormReady();}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {isFormReady();}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {isFormReady();}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        edtPhoneNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {isFormReady();}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent RegistrationActivity = new Intent(RegistrationActivity.this, MainActivity.class);
                startActivity(RegistrationActivity);
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent registration_question_Ans_Activity_intent = new Intent(RegistrationActivity.this, RegistrationQuestionAnsActivity.class);

                registration_question_Ans_Activity_intent.putExtra("name",name );
                registration_question_Ans_Activity_intent.putExtra("phone", phone);
                registration_question_Ans_Activity_intent.putExtra("email",email);
                registration_question_Ans_Activity_intent.putExtra("password",password);
                startActivity(registration_question_Ans_Activity_intent);
            }
        });
    }

    public  void isFormReady(){
        name=edtName.getText().toString();
        email=edtEmail.getText().toString();
        password=edtPassword.getText().toString();
        phone=edtPhoneNo.getText().toString();
        if(!password.equals("")&&!email.equals("")&&!name.equals("")&&!phone.equals(""))
            btn_next.setEnabled(true);
        else
            btn_next.setEnabled(false);
    }

    private class Asynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                URLConnection conn=obj.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(urls[0]);
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
        @Override
        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            try
            {
                jsonResponse = new JSONObject(result);
                JSONArray jsonMainNode = jsonResponse.optJSONArray("info");
                int length = jsonMainNode.length();
                question1= new String[length];
                question2= new String[length];
                que1_id=new int[length];
                que2_id=new int[length];
                int id;
                for(int i=0; i < length; i++)
                {
                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    id = jsonChildNode.getInt("que_id");

                    que1_id[i]= jsonChildNode.getInt("que_id");
                    if(id<i)
                    {
                        que2_id[i] = id;
                        question2[i]=jsonChildNode.get("question").toString();
                    }
                    else
                    {
                        que1_id[i] = id;
                        question1[i]=jsonChildNode.get("question").toString();
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


	        /*if(listobj!=null)
	        {

	        }
	        else
	        {
	        	Toast.makeText(getApplicationContext(),"No data to display", Toast.LENGTH_LONG).show();
	        }*/
        }
    }

}

