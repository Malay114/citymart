package com.example.malay.citymart;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import bean.ListItem;


public class ShopListAdapter extends ArrayAdapter<ListItem>
{
    private final Activity context;
    private int lastPosition = -1;
    private List<ListItem> categList;
    public ShopListAdapter(Activity context, List<ListItem> categList)
    {
        super(context, R.layout.category_custom_list);
        this.context = context;
        this.categList=categList;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categList.size();
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {

        Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.shop_result_list, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_shop_name);
        TextView txtKm=(TextView)rowView.findViewById(R.id.txt_km);
        RatingBar ratingbar=(RatingBar)rowView.findViewById(R.id.ratingbar1);
        TextView offers=(TextView)rowView.findViewById(R.id.tex_offers);
        ListItem singleItem=categList.get(position);
        txtTitle.setText(singleItem.getName());
        txtKm.setText(singleItem.getKm()+"  "+singleItem.getTime());


        if(SplashActivity.active.equals("Active"))
        {
            offers.setText(singleItem.getOffers());
        }
        else
        {
            offers.setText("   ");
        }
        if(LoginActivity.offer==1)
        {
            offers.setText(singleItem.getOffers());
        }
        else
        {
            offers.setText("   ");
        }
        //offers.setText(singleItem.getOffers());
        ratingbar.setRating(singleItem.getRating());
        rowView.startAnimation(animation);
        lastPosition = position;
        return rowView;
    }
}
