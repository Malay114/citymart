package com.example.malay.citymart;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import utility.Path;
import utility.Preferences;

public class SplashActivity extends Activity

{
    static String active="";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity_main);
        boolean pr= PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED;
        if(!pr)
            requestPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,1);
        if(PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
            requestPermissions(Manifest.permission.ACCESS_FINE_LOCATION,1);
        Preferences settings = new Preferences(getApplicationContext()) ;
        Path.setPath("http://spcitymart.000webhostapp.com/citymartwebservice/");
//        Path.setPath("http://spcitymart.host22.com/citymartwebservice/");



        String servername= Path.getPath();
        String str_url=servername+"city_name.php";
        //String str_url="http://192.168.1.9/php/citymartwebservice/city_name.php";

        new Asynk_getCityName().execute(str_url);

    }
    private void requestPermissions(String PermissionName, int PermissionReuestCode){
        ActivityCompat.requestPermissions(this,new String[]{PermissionName},PermissionReuestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
                if(grantResults.length>0 & grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    return;
                }

        }
    }
    private class Asynk_getCityName extends AsyncTask<String, Void, Void>
    {

        @Override
        protected Void doInBackground(String... url) {
            // TODO Auto-generated method stub
            BufferedReader reader;
            StringBuilder sb = new StringBuilder();
            String line = null;
            try
            {
                URL obj=new URL(url[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( url[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String result=sb.toString();

            String city_name[]=null;
            int city_id[]=null;
            JSONObject jsonResponse;

            SqliteHelper sqlitehelper = new SqliteHelper(SplashActivity.this);
            try
            {
                jsonResponse = new JSONObject(result);
                JSONArray jsonMainNode = jsonResponse.optJSONArray("info");
                int length = jsonMainNode.length();
                city_name= new String[length];
                city_id= new int[length];
                sqlitehelper.ifexist();
                for(int i=0; i < length; i++)
                {
                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    city_name[i]  = jsonChildNode.get("city_name").toString();
                    city_id[i]  = jsonChildNode.getInt("city_id");
                    sqlitehelper.createCity(new MyObject(city_id[i],city_name[i]));
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run() {
                    try
                    {

                        SqliteHelper sqliteHelper=new SqliteHelper(getApplicationContext());
                        //String email=sqliteHelper.getEmail();
                        //String pwd=sqliteHelper.GetPassword();
                        //active=sqliteHelper.ActiveOrNot();
                        //Log.e("","email"+email);

                        Intent SplashActivityIntent;

//                        if((email.equals("") && pwd.equals("") &&active.equals("")) || active.equals("'DeActive"))
//                        {
////                            SplashActivityIntent = new Intent(SplashActivity.this, RegistrationActivity.class);
//                            SplashActivityIntent = new Intent(SplashActivity.this, MainActivity.class);
//                        }
//                        else
//                        {
                            SplashActivityIntent = new Intent(SplashActivity.this, MainActivity.class);
//                        }
                        startActivity(SplashActivityIntent);
                        finish();
                    }catch(Exception e){
                    }
                }
            },1000);
            super.onPostExecute(result);
        }
    }
    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}

