package com.example.malay.citymart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utility.Path;


public class LoginActivity extends Activity {

    String cityName;
    int cityId;
    String uname,password;
    EditText txtUserName,txtPassword;
    TextView txtMessage;
    static String status;
    private ProgressDialog progress;
    static int offer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login);

        txtUserName=(EditText)findViewById(R.id.edt_userName);
        txtPassword=(EditText)findViewById(R.id.Password);
        txtMessage=(TextView)findViewById(R.id.txt_message);
        Button btnLogin=(Button)findViewById(R.id.btn_Login);
    	/*progress = new ProgressDialog(this);
        progress.setMessage("Loading... ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCanceledOnTouchOutside(false);
        //progress.setContentView(R.layout.custom_progress_background);
        progress.setIndeterminate(true);
        progress.show();*/


        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                uname=txtUserName.getText().toString();
                password=txtPassword.getText().toString();

                if (!isValidEmail(uname)) {

                    txtMessage.setText("Invalid Email");
                    txtUserName.setText("");
                    txtPassword.setText("");
                    txtUserName.setFocusable(true);
                }

                if (!isValidPassword(password)) {
                    txtMessage.setText("Password must be greater then 6 characters");
                    txtPassword.setText("");
                    txtPassword.setFocusable(true);
                }
                offer=1;

                String serverName= Path.getPath();
                String str_url=serverName+"app_user_login.php?user_name="+uname+"&password="+password;
                //String str_url="http://192.168.1.26/php/citymartwebservice/app_user_login.php?user_name="+uname+"&password="+password;

                new Asynkclass().execute(str_url);

            }
        });


    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 1) {
            return true;
        }
        return false;
    }

    private class Asynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            BufferedReader reader;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(url[0]);
                URLConnection conn1=obj.openConnection();
                reader= new BufferedReader(new InputStreamReader(conn1.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String result=sb.toString();


            JSONObject jsonResponse;


            try
            {
                jsonResponse = new JSONObject(result);
                JSONArray jsonMainNode = jsonResponse.optJSONArray("info");
                Log.e("",""+result);

                int length = jsonMainNode.length();
                Log.e("",""+length);

                for(int i=0; i < length; i++)
                {
                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    status  = jsonChildNode.get("status").toString();
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return status;

        }
        @Override
        protected void onPostExecute(String result) {


            if(result.equals("success"))
            {
                SqliteHelper sqliteHelper=new SqliteHelper(getApplicationContext());
                sqliteHelper.InsertLogin(uname, password);
                //progress.hide();
                Intent SplashActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                Log.e("login","uname="+txtUserName.getText().toString()+"password"+txtPassword.getText().toString());
                startActivity(SplashActivityIntent);
                finish();
            }

            SqliteHelper sqliteHelper=new SqliteHelper(getApplicationContext());
            String email=sqliteHelper.getEmail();
            String pwd=sqliteHelper.GetPassword();
            String active=sqliteHelper.ActiveOrNot();

			/*if(active.equalsIgnoreCase("Active"))
			{
					if(!(uname.equalsIgnoreCase(email)) || !(password.equalsIgnoreCase(pwd)))
					{
						txtPassword.setText("");
						txtUserName.setText("");
						txtMessage.setText("Incorrect User name or Password");
					}
					else if(!(uname.equalsIgnoreCase(email)))
					{
						txtUserName.setText("");
						txtPassword.setText("");
						txtMessage.setText("Incorrect User name or Password");
					}
					if(uname.equalsIgnoreCase(email) && password.equalsIgnoreCase(pwd))
					{

			            Intent SplashActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
			            Log.e("login","uname="+txtUserName.getText().toString()+"password"+txtPassword.getText().toString());
		                startActivity(SplashActivityIntent);
		                finish();
					}
			}
			else
			{
				sqliteHelper.InsertLogin(uname,password);
			}*/
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        }

    }
    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}
