package com.example.malay.citymart;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bean.ListItem;
import utility.Util;

public class CategoryListAdapter extends ArrayAdapter<ListItem>
{
    private final Activity context;
    private int lastPosition = -1;
    private List<ListItem> categList;
// test push
    public CategoryListAdapter(Activity context, List<ListItem> categList)
    {
        super(context, R.layout.category_custom_list);
        this.context = context;
        this.categList=categList;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categList.size();
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {

        //Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);

        Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.single_custom_list_view, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_name);
        TextView txtdesc=(TextView)rowView.findViewById(R.id.txt_desc);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.image1);
        ListItem singleItem=categList.get(position);
        txtTitle.setText(singleItem.getName());
        txtdesc.setText(singleItem.getDescOrAddress());
        imageView.setImageBitmap(Util.StringToBitmap(singleItem.getImageData()));
        rowView.startAnimation(animation);
        lastPosition = position;

        return rowView;

    }
}