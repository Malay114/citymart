package com.example.malay.citymart;


public class MyObject {

    public String objectName;
    public int objectId;

    // constructor for adding sample data
    public MyObject(int objectId,String objectName){

        this.objectName = objectName;
        this.objectId=objectId;
    }

    @Override
    public String toString() {
        return  objectName;
    }


}

