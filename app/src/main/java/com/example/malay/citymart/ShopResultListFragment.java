package com.example.malay.citymart;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bean.ListItem;
import utility.Path;
import utility.Util;



public class ShopResultListFragment extends Fragment implements View.OnClickListener {
    View view;
    private ProgressDialog progress;
    private ListView list;
    private List<ListItem> listobj;
    ShopListAdapter adapter;
    FragmentManager fragmentManager;
    FragmentTransaction ft;
    Fragment frag_ShopAddres_list;
    Bundle args;
    //GridView list;

    private String Subcategory_Name;
    private String cityName;
    private String Subcategory_Id;
    private String ImageData;

    public Location location;
    public double MyLat, MyLong,dis;
    String distance = "";
    String duration = "";
    Double User_latitude ,User_longitude;
    private Button msort;
    int cityId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.subcategory_custom_list, container, false);
        Subcategory_Id = getArguments().getString("Subcategory_Id");
        Subcategory_Name = getArguments().getString("Subcategory_Name");
        ImageData=getArguments().getString("imageData");
        cityId=getArguments().getInt("city_id",0);

        args = new Bundle();
        fragmentManager= getFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();

        GetCurrentLocation getlocation=new GetCurrentLocation();
        Context context=getContext();
        getlocation.turnGPSOn(context); // method to turn on the GPS if its in off state.

        getlocation.getMyCurrentLocation(context);
        User_latitude =getlocation.MyLat;
        User_longitude =getlocation.MyLong;
        getlocation.turnGPSOff(context);



//        if(Geocoder.isPresent()){
//            try {
//                String location = MainActivity.city;
//                Geocoder gc = new Geocoder(getContext());
//                List<Address> addresses= gc.getFromLocationName(location, 5); // get the found Address Objects
//
//                List<LatLng> ll = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available
//                for(Address a : addresses){
//                    if(a.hasLatitude() && a.hasLongitude()){
//                        User_latitude=a.getLatitude();
//                        User_longitude= a.getLongitude();
//                    }
//                }
//            } catch (IOException e) {
//                // handle the exception
//            }
//        }

        String serverName= Path.getPath();
        String str_url=serverName+"get_shop_list_for_subcategory_distance.php?city_id="+cityId+"&subcategory_id="+Subcategory_Id+"&user_latitude="+User_latitude+"&user_longitude="+User_longitude;

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Loading... ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCanceledOnTouchOutside(false);
        progress.setIndeterminate(true);
        progress.show();

        list=(ListView) view.findViewById(R.id.list1);
        new Asynkclass().execute(str_url);

        msort= (Button)view.findViewById(R.id.btn_sort);
        msort.setOnClickListener(this);
        return view;
    }



    public static Comparator<ListItem> KmAscComparator = new Comparator<ListItem>()
    {
        @Override
        public int compare(ListItem arg0, ListItem arg1) {
            // TODO Auto-generated method stub
            Double km1 = arg0.getKm();
            Double km2 = arg1.getKm();

            return km1.compareTo(km2);
        }
    };

    //Comparator for Descending Order
    public static Comparator<ListItem> KmDescComparator = new Comparator<ListItem>() {

        @Override
        public int compare(ListItem arg0, ListItem arg1) {
            // TODO Auto-generated method stub
            Double km1 = arg0.getKm();
            Double km2 = arg1.getKm();
            return km2.compareTo(km1);
        }
    };

    public static Comparator<ListItem> RatingComparator = new Comparator<ListItem>() {
        @Override
        public int compare(ListItem arg0, ListItem arg1) {
            // TODO Auto-generated method stub
            float rating1 = arg0.getRating();
            float rating2 = arg1.getRating();
            return Float.compare(rating1, rating2);
        }
    };
    public static Comparator<ListItem> RatingComparator1 = new Comparator<ListItem>() {
        @Override
        public int compare(ListItem arg0, ListItem arg1) {
            // TODO Auto-generated method stub
            float rating1 = arg0.getRating();
            float rating2 = arg1.getRating();
            return Float.compare(rating2, rating1);
        }
    };


    private class Asynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            // TODO Auto-generated method stub
            BufferedReader reader=null;

            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0]);
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }
        @Override
        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            listobj = new ArrayList<ListItem>();

            try
            {
                jsonResponse = new JSONObject(result);
                listobj= Util.jsonTOListItemFinalResult(jsonResponse);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();

            if(listobj!=null)
            {
                adapter = new ShopListAdapter(getActivity(), listobj);
                list.setAdapter(adapter);

                list.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        // TODO Auto-generated method stub
                        frag_ShopAddres_list=new ShopAddressFragment();
                        args.putString("Shop_Id",listobj.get(position).getId());
                        args.putString("Shop_Type",listobj.get(position).getType());
                        args.putString("Shop_Name",listobj.get(position).getName());
                        args.putFloat("Rating",listobj.get(position).getRating());
                        args.putDouble("Counter",listobj.get(position).getCount());
                        args.putDouble("Longitude",listobj.get(position).getLongitude());
                        args.putDouble("Latitude",listobj.get(position).getLatitude());
                        args.putInt("city_id",cityId);
                        frag_ShopAddres_list.setArguments(args);
                        fragmentManager.beginTransaction()
                                .replace(R.id.content_main, frag_ShopAddres_list )
                                .addToBackStack("")
                                .commit();
                    }

                });
            }
            else
            {
                Toast.makeText(getContext(), "No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub

        PopupMenu popup = new PopupMenu(getActivity(), msort);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Sort by Km ASC"))
                {
                    Collections.sort(listobj, KmAscComparator);
                }
                else if(item.getTitle().equals("Sort by KM DES"))
                {
                    Collections.sort(listobj, KmDescComparator);
                }
                else if(item.getTitle().equals("Sort by Rating"))
                {
                    Collections.sort(listobj, RatingComparator1);
                }
                adapter.notifyDataSetChanged();

                return true;
            }
        });
        popup.show();

    }
}
