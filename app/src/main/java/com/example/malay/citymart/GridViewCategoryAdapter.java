package com.example.malay.citymart;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bean.ListItem;
import utility.Util;

public class GridViewCategoryAdapter extends ArrayAdapter<ListItem>
{
    private final Activity context;
    private int lastPosition = -1;
    private List<ListItem> categList;
    public GridViewCategoryAdapter(Activity context, List<ListItem> categList)
    {
        super(context, R.layout.grid_view_activity);
        this.context = context;
        this.categList=categList;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categList.size();
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {

        Animation animation = AnimationUtils.loadAnimation(getContext(), ((position%2 )==0) ? R.anim.right_from_left : R.anim.left_from_right);

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.grid_view_list, null);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_name_grid);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imagegrid1);
        ListItem singleItem=categList.get(position);
        txtTitle.setText(singleItem.getName());

        imageView.setImageBitmap(Util.StringToBitmap(singleItem.getImageData()));
        rowView.startAnimation(animation);
        lastPosition = position;
        return rowView;

    }
}