package com.example.malay.citymart;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import utility.Path;


public class RegistrationQuestionAnsActivity extends Activity implements AdapterView.OnItemClickListener {

    EditText edtAns1,edtAns2;
    static String ans1,ans2;
    Spinner spn_Question1,spn_Question2;
    RadioButton shop,simple;
    private String type;
    static int q1,q2;
    String que1,que2;
    String name,email,password,phone;
    Button btn_register, btn_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.registration_que_ans);

        edtAns1=(EditText)findViewById(R.id.edt_answer1);
        edtAns2=(EditText)findViewById(R.id.edt_answer2);
        spn_Question1=(Spinner)findViewById(R.id.spnier_q1);
        spn_Question2=(Spinner)findViewById(R.id.Spnier_q2);

        Intent intent=getIntent();
        name=intent.getStringExtra("name");
        phone=intent.getStringExtra("phone");
        email=intent.getStringExtra("email");
        password=intent.getStringExtra("password");

        edtAns1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {isFormReady();}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        edtAns2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {isFormReady();}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
    	/*ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, RegistrationActivity.question1);
    	spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	spn_Question1.setAdapter(spinnerArrayAdapter);

    	ArrayAdapter<String> spinnerArrayAdapter_q2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, RegistrationActivity.question2);
    	spinnerArrayAdapter_q2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	spn_Question1.setAdapter(spinnerArrayAdapter_q2);*/

        spn_Question1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                // TODO Auto-generated method stu
                que1=spn_Question1.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
                que1=spn_Question1.getSelectedItem().toString();
            }
        });

        spn_Question2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                // TODO Auto-generated method stu
                que2=spn_Question1.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
                que2=spn_Question2.getSelectedItem().toString();
            }
        });

        btn_register=(Button)findViewById(R.id.btn_Register);
        btn_back=(Button)findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent Registration_que_ansIntent = new Intent(RegistrationQuestionAnsActivity.this, RegistrationActivity.class);
                startActivity(Registration_que_ansIntent);
                finish();
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ans1=edtAns1.getText().toString();
                ans2=edtAns2.getText().toString();
                String serverName= Path.getPath();
//                String url="?name="+name+"&phoneNo="+phone+"&uname="+email+"&password="+password;
//                url+="&question1="+que1+"&ans1="+ans1+"&question2="+que2+"&ans2="+ans2+"&user_type=Simple";
                String str_url=serverName+"app_user_registration.php?name="+name+"&phoneNo="+phone+"&uname="+email+"&password="+password+"&question1="+que1+"&ans1="+ans1+"&question2="+que2+"&ans2="+ans2+"&user_type=Simple";
                //String str_url="http://192.168.1.26/php/citymartwebservice/app_user_registration.php"+url;
                new Asynkclass().execute(str_url);

//                Intent Registration_que_ansIntent = new Intent(RegistrationQuestionAnsActivity.this, LoginActivity.class);
//                startActivity(Registration_que_ansIntent);
//                finish();
            }
        });

    }
    public  void isFormReady(){
        ans1=edtAns1.getText().toString();
        ans2=edtAns1.getText().toString();
        if(!ans1.equals("")&&!ans2.equals(""))
            btn_register.setEnabled(true);
        else
            btn_register.setEnabled(false);
    }
    private class Asynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls) {
            // TODO Auto-generated method stub
            BufferedReader reader;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

//                InputStream input = new BufferedInputStream(obj.openStream());
//                OutputStream output = new FileOutputStream(urls[0]);

                reader= new BufferedReader(new InputStreamReader(obj.openStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String result=sb.toString();
            JSONObject jsonResponse;
            String status = null;

            try {
                jsonResponse = new JSONObject(result);
                JSONArray jsonMainNode = jsonResponse.optJSONArray("info");
                int length = jsonMainNode.length();
                for(int i=0; i < length; i++)
                {
                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    status  = jsonChildNode.get("status").toString();
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {

            // TODO Auto-generated method stub
            super.onPostExecute(result);
			if(result.equals("success")) {
				Intent RegistrationActivityIntent = new Intent(RegistrationQuestionAnsActivity.this, LoginActivity.class);
                startActivity(RegistrationActivityIntent);
                finish();
			}
     		else {
				Toast.makeText(getApplicationContext(), "Registration Error.....", Toast.LENGTH_LONG).show();
			}
        }
    }
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub
    }
}
