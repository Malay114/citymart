package bean;

public class ListItem 
{
	private String name;
	private String descOrAddress="";
	private String imageData;
	private String id;
	private double km;
	private double latitude;
    private double longitude;
    private String time;
    private String type;
    private String Question;
    private float rating;
    private String Offers;
    private int count;
	
	
	public ListItem(String name, String descOrAddress, float rating,
					String imageData, String id) {
		
		this.name = name;
		this.descOrAddress = descOrAddress;
		this.rating = rating;
		this.imageData = imageData;
		this.id=id;
		this.km=km;
		this.latitude=latitude;
		this.longitude=latitude;
		this.time=time;
		this.type=type;
	}
	public ListItem() {
		name = "";
		descOrAddress = "";
		rating = 0;
		imageData = "";
		id="";
		km=0;
		latitude=0;
		longitude=0;
		time="";
		type="";
		
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		
		this.name = name;
	}
	public String getDescOrAddress() {
		return descOrAddress;
	}
	public void setDescOrAddress(String descOrAddress) {
		this.descOrAddress = descOrAddress;
	}
	
	public String getImageData() {
		return imageData;
	}
	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getKm() {
		return km;
	}
	public void setKm(double km) {
		this.km = km;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getQuestion() {
		return Question;
	}
	public void setQuestion(String question) {
		Question = question;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public String getOffers() {
		return Offers;
	}
	public void setOffers(String offers) {
		Offers = offers;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
